At the time I was writing many applications that all used various API keys. Each time I had the same issue that I wanted to keep the API keys out of the repository so I wrote this class to warehouse them all.

It acts as a key, value store where I ask for an API key by name from the main application and it returns it. The mechanism of how it finds it is not relevant to the calling application.