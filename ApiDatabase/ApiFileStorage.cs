﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace ApiDatabase
{
	/// <summary>
	/// Problem: Writing many programs that use API keys results in writing the same code to load in API keys from an external file (so as to not include them in the git repository).
	/// Intent: To allow the storage of all API keys I have in one place, externally to any git repository and to allow the retrieval of those keys by name.
	/// </summary>
	public class ApiFileStorage : IApiStorage
	{
		private const string DEFAULT_API_FILE = "default.keydb";
		private string StorageLocation { get; }
		private Dictionary<string, string> ApiKeys { get; set; }
		private Action<string> LogCallback { get; }

		/// <summary>
		/// Create an instance of the <see cref="ApiFileStorage"/> class.
		/// This allows api keys for various services to be stored centrally and retrieved by name.
		/// </summary>
		/// <param name="storageLocation">File path for the key file. If none is provided it will create a default one in the local directory called default.keydb</param>
		/// <param name="logCallback">Optional method to provided to get logging information</param>
		public ApiFileStorage(string storageLocation = null, Action<string> logCallback = null)
		{
			LogCallback = logCallback;
			if (!string.IsNullOrWhiteSpace(storageLocation))
			{
				StorageLocation = storageLocation;
				LogCallback?.Invoke($"Loading in key database file {storageLocation}");
			}
			else
			{
				StorageLocation = DEFAULT_API_FILE;
				if (!File.Exists(StorageLocation))
				{
					LogCallback?.Invoke("Can't find default api key file and no filepath loaded in, creating a default one");
					CreateDefualtApiKeyDatabase();
				}
			}
			LoadKeyFile(StorageLocation);
		}

		/// <summary>
		/// Gets a key from the database by name
		/// </summary>
		/// <param name="name">The name of the key to return.</param>
		/// <exception cref="FileNotFoundException">Could not find the key filename passed in on creation</exception>
		/// <returns>The key if found, else null.</returns>
		public string GetApiKey(string name)
		{
			if (ApiKeys == null)
				return null;
			return ApiKeys.ContainsKey(name) ? ApiKeys[name] : null;
		}

		private void LoadKeyFile(string filePath)
		{
			if (File.Exists(filePath))
			{
				ApiKeys = JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText(filePath));
				LogCallback?.Invoke($"Found API key database. Loaded in keys for {string.Join(", ", ApiKeys.Select(x => x.Key))}.");
			}
			else
			{
				LogCallback?.Invoke($"Failed to find file {filePath}");
				throw new FileNotFoundException("Could not locate the file name passed in.", filePath);
			}
		}

		private void CreateDefualtApiKeyDatabase()
		{
			Dictionary<string, string> defaultApiKeys = new Dictionary<string, string> { { "ExampleServiceName1", "ExampleKey1" }, { "ExampleServiceName2", "ExampleKey2" } };
			string jsonOutput = JsonConvert.SerializeObject(defaultApiKeys, Formatting.Indented);
			using (TextWriter tw = new StreamWriter(DEFAULT_API_FILE))
			{
				tw.Write(jsonOutput);
			}
		}
	}
}
