﻿namespace ApiDatabase
{
	public interface IApiStorage
	{
		string GetApiKey(string name);
	}
}
